# Hörsaalübung 11

## Description
+ Hörsaalübung 11 für GDI
+ Themen:
    - Python Data Example & Basics
    - Python Rechteck Class Example

## Installation & Usage
Jupyter Notebook Insatz des HRZ der TUDa -> https://tu-jupyter-iib.ca.hrz.tu-darmstadt.de/hub/home 
(Jupyter Notebook und Python Installation notwendig)

## Authors and acknowledgment
MGehring

## License
No License, free to use for educational purpose.

## Project status
Finished
