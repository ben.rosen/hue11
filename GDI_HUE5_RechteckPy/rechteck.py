class Rechteck:

    a = 0.0
    b = 0.0
    flaeche = 0.0
    umfang = 0.0

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def BerechneFlaeche(self):
        neueFlaeche = self.a * self.b
        self.flaeche = neueFlaeche

    def GebeUmfangAus(self):
        umfang =  2*self.a + 2*self.b
        return umfang

def main():
    rechteck = Rechteck(3,4)
    print(f"Länge Seite a = {rechteck.a}")
    print("Länge Seite b = %s" %(rechteck.b))
    print(rechteck.flaeche)
    rechteck.BerechneFlaeche()
    print(rechteck.flaeche)
    print("Fläche des Rechtecks: %s \nUmfang des Rechtecks: %s" %(rechteck.flaeche, rechteck.GebeUmfangAus()))

if __name__ == "__main__":
    main()
    